#!/usr/bin/env python
"""
misc.py - Misc. word war bot functions
Author: Me
About: http://jesus.thewrigro.com
"""

import energy, random, urllib, re, db, wordgen, login
import rconfig
config = rconfig.config
from flags import AdminOnly, WithFlag
from functions import *

@Channel_Only
def f_faq (self, origin, match, args):
    """*Provide FAQ link*, Usage: ``!faq``.

- *!faq*       Provides the FAQ link to people via private message.

"""
    if origin.sender == '#nanowrimo':
        self.msg(origin.nick, "FAQ: Most of your questions are answered in the FAQ. A link may be found in the topic. If your question is not there, please feel free to PM a channel operator and ask them.")

f_faq.rule = (['faq'], r'.*')

@AdminOnly
def f_op (self, origin, match, args):
	self.write(('MODE', '#nano24hrwar +o Lily'))

f_op.rule = (['op'], 'r.*')

@WithFlag('d')
def f_join(self, origin, match, args):
    """*Join a channel*, Usage: ``!join [channel]``.

- *!join <#channel>*       Joins the channel <#channel>, storing data in config.channels_in.

"""
    config.channels_in.append(match.group(1))
    self.write(('JOIN', match.group(1)))

f_join.rule = (['join'], r'(#.*)')

@NoDoc
@WithFlag('d')
@PM_Only
def f_whereami(self, origin, match, args):
    """*List my channels*, Usage: ``!whereami``.

- *!whereami*       Return the data found in config.channels_in.

"""
    self.msg(origin.nick, "WHERE: Currently, I'm hanging out in: %s." % ', '.join(config.channels_in))

f_whereami.rule = (['whereami'], r'(.*)')

@NoDoc
@WithFlag('d')
def f_defaultchannels(self, origin, match, args):
    """*List configured default channels*, Usage: ``!defaultchannels``.

    - *!defaultchannels*        Return the data found in config.channels.
    """
    self.msg(origin.sender, "WHERE: I'm configured to join {}.".format(', '.join(config.channels)))

f_defaultchannels.rule = (['defaultchannels'], r'(.*)')

@NoDoc
@AdminOnly
@PM_Only
def f_say (self, origin, match, args):
    """*Say something in a channel*, Usage: ``!say``.

- *!say #<channel> <text>*       Say <text> in <channel>.

"""
    self.msg(match.group(1), match.group(2))

f_say.rule = (['say'], r'(#[^ ]*) (.*)')

@WithFlag('d')
@PM_Only
def f_test(self, origin, match, args):
    """*Echo text*, Usage: ``!test [text]``.

- *!test <text>*       Echo text <text> back to you.

"""
    self.msg(origin.nick, "Test was: '%s'" % repr(match))

f_test.rule = (['test'], r'(.*)')

@WithFlag('d')
def f_part(self, origin, match, args):
    """*Leave a channel*, Usage: ``!part [channel]``.

- *!part <#channel>*       Parts the channel <#channel>, storing data in config.channels_in.

"""
    config.channels_in = [x for x in config.channels_in if x != match.group(1)]
    self.write(('PART', match.group(1)))

f_part.rule = (['part'], r'(#.*)')

@AdminOnly
def f_nick(self, origin, match, args):
    """*Change nickname*, Usage: ``!nick [nick]``.

- *!nick <nick>*       Changes current nickname to <nick>.

"""
    self.write(('NICK', match.group(1)))

f_nick.rule = (['nick'], r'(.*)')

@AdminOnly
def f_changepass(self, origin, match, args):
    """*Change a password*, Usage: ``!changepass [user]``.

- *!changepass <user>*       Change's the password for <user> to 'changeme'.

"""
    wdb = db.getcursor()
    wdb.execute("UPDATE users SET password=? WHERE name=?", (login.password('changeme'), match.group(1)))
    self.msg(origin.nick, "Changed %s's password to 'changeme'." % match.group(1))

f_changepass.rule = (['changepass'], r'([^ ]*)')

@AdminOnly
def f_ident(self, origin, match, args):
    """*Force identification*, Usage: ``!ident``

- *!ident*       The bot will identify to NickServ, ChanServ, restore its modes and vhost
"""
    nickpass = file(self.datadir + '/nick.pass').readline().strip()
    self.msg("NickServ", "identify %s" % nickpass)
    self.msg("ChanServ", "op")
    self.write(("MODE", "%s +BTp" % self.nick))
    vhostpass = file(self.datadir + '/vhost.pass').readline().strip()
    self.write(("VHOST" , "jesus %s" % vhostpass))

f_ident.rule = (['ident'], r'(.*)')

@Energetic (50, 5)
def f_convert(self, origin, match, args):
    """*Conversion functions*, Usage: ``!con[f|v] [#[f|c|kg|lb|km|mi]]``

- *!conf/!conv [#[f/c]]*      Convert # in Fahrenheit (f) or Centigrade (c) to Centigrade or Fahrenheit.
- *!conf/!conv [#[kg/lb]]*       Convert # in Kilograms (kg) or Pounds (lb) to Pounds or Kilograms.
- *!conf/!conv [#[km/mi]]*       Convert # in Kilometres (km) or Miles (mi) to Miles or Kilometres.
"""
    init = re.compile("([-0-9]+\.?[0-9]?[0-9]?)[0-9]*")
    if match.group(2).lower() == "c":
        # Convert celsius to fahrenheit
        self.msg(origin.sender, "CONVERT: %s Celsius is %s Fahrenheit." % (match.group(1), init.match(str((float(match.group(1)) * 1.8) + 32)).group(1)))
    elif match.group(2).lower() == "f":
        # Convert fahrenheit to celsius
        self.msg(origin.sender, "CONVERT: %s Fahrenheit is %s Celsius." % (match.group(1), init.match(str((float(match.group(1)) - 32) / 1.8)).group(1)))
    elif match.group(2).lower() == "km":
        # Convert kilometers to miles
        self.msg(origin.sender, "CONVERT: %s kilometers is roughly %s miles." % (match.group(1), init.match(str(float(match.group(1)) / 1.609)).group(1)))
    elif match.group(2).lower() == "mi":
        # Convert kilometers to miles
        self.msg(origin.sender, "CONVERT: %s miles is roughly %s kilometers." % (match.group(1), init.match(str(float(match.group(1)) * 1.609)).group(1)))
    elif match.group(2).lower() == "lb":
        # Convert pounds to kilograms
        self.msg(origin.sender, "CONVERT: %s pounds is roughly %s kilograms." % (match.group(1), init.match(str(float(match.group(1)) / 2.204)).group(1)))
    elif match.group(2).lower() == "kg":
        # Convert kilograms to pounds
        self.msg(origin.sender, "CONVERT: %s kilograms is roughly %s pounds." % (match.group(1), init.match(str(float(match.group(1)) * 2.204)).group(1)))

f_convert.rule = (['conf', 'conv', 'convert'], '([-0-9.]+)([Cc]|[Ff]|(?:lb|LB|lB|Lb)|(?:kg|KG|kG|Kg)|(?:km|KM|kM|Km)|(?:mi|MI|mI|Mi))')

@AdminOnly
def f_ignore(self, origin, match, args):
    """*Blacklist a user*, Usage: ``!ignore [mask]``

- *!ignore [mask]*      Add [mask] to the ignore list. Mask is in the following format: <nick>!<ident>@<host>.

"""
    if re.compile("(.*)!(.*)@(.*)").match(match.group(1)):
        self.adduser(match.group(1))
        self.msg(origin.nick, "BLACKLIST: Added %s to the blacklist. Will wear off on !rehash/!reload." % match.group(1))

f_ignore.rule = (['ignore'], r'(.*)')

@AdminOnly
def f_unignore(self, origin, match, args):
    """*Wipe the blacklist*, Usage: ``!unignore``

- *!unignore*       Empties the blacklist.

"""
    self.blacklist = []
    self.msg(origin.nick, "BLACKLIST: Emptied the blacklist.")
f_unignore.rule = (['unignore'], r'(.*)')

@AdminOnly
def f_showlist(self, origin, match, args):
    """*Show the blacklist*, Usage: ``!showlist``

- *!showlist*       Displays the content of the blacklist.

"""
    blist = self.getlist()
    self.msg(origin.nick, "Current blacklist (fmt: * n (i@h))")
    for user in blist:
        self.msg(origin.nick, "* %s (%s)" % (user["nick"], user["hostmask"]))

f_showlist.rule = (['showlist'], r'(.*)')

@AdminOnly
def f_limit(self, origin, match, args):
    """*Raise/display flood limit*, Usage: ``!limit [#]``

- *!limit [#]*       Change the limit to [#]
- *!limit*       Display the current flood limit

"""
    if match.group(1).isdigit():
        self.floodcheck["limit"] = match.group(1)
        self.msg(origin.nick, "Flood limit is now: %s." % match.group(1))
    elif match.group(1) == "":
        self.msg(origin.nick, "The current limit is: %s." % self.floodcheck["limit"])
    else:
        self.msg(origin.nick, "'%s' is an invalid limit. Try again." % match.group(1))

f_limit.rule = (['limit'], r'(.*)')

@Energetic (75, 15)
def f_ping(self, origin, match, args):
    """*Ping (status check)*, Usage: ``!ping``

- *!ping*        Request a response from the bot to ensure that it's still online.

"""
    self.msg(origin.sender, 'Pong! %s' % match.group(1))

f_ping.rule = (['ping'], r'(.*)')

@Ignore_Channel_Only
@Energetic (75, 25)
def f_pick(self, origin, match, args):
    """*Pick from a list*, Usage: ``!pick something, somethingelse{, anothersomething ...}``

- *!pick something, somethingelse{, anothersomething ...}*       Pick one of the options listed at random.

"""
    list_string = match.group(1)

    # First, try splitting on commas.  Otherwise, try splitting on
    # pipes.
    pick_list = [x.strip() for x in list_string.split(",") if len(x) > 0]

    if len(pick_list) < 2:
       pick_list = [x.strip() for x in list_string.split("|") if len(x) > 0]

    if len(pick_list) < 2:

        # Neither worked.  Complain.
        self.msg(origin.sender, "PICK: %s, I need more than one thing to pick from." % (origin.nick))
        return
    picked = random.choice(pick_list)
    self.msg(origin.sender, "PICK: %s, I choose |%s|!" % (origin.nick, picked))

f_pick.rule = (['pick'], r'(.*)')

@Energetic (75, 15)
def f_rand(self, origin, match, args):
    """*Random number*, User: ``!rand [#] [#]``

- *!random <#> <#>*       Returns a random number between <#> and <#>.

"""
    if len(match.groups()) == 2:
        start, stop = match.groups()
    else:
        self.msg(origin.sender, "RAND: Invalid value for rand.")
    picked = random.choice(xrange(int(start), int(stop)+1))
    self.msg(origin.sender, "RAND: %s, the random value is |%s|!" % (origin.nick, picked))

f_rand.rule = (['rand'], r'([0-9]+),? ([0-9]+)')

@Energetic (1, 1)
def f_roll(self, origin, match, args):
    """*Dice roll*, ``Usage: !roll [#d##|#dF|#dP|#dW|1dWC]``

- *!roll #d##*       Roll a ##-sided die # times.
- *!roll #dF*       Roll # Fudge dice.
- *!roll #dP*       Roll # the Pool dice.
- *!roll #dW*       Roll # World of Darkness dice.
- *!roll 1dWC*       Make a World of Darkness Chance Roll.

"""
    dice = int(match.group(1))
    if dice > 64:
        self.msg(origin.sender, "DICE: %s, the number of dice at a time is limited to 64." % (origin.nick))
        return
    sides = match.group(2).lower()
    if "f" == sides:
        roll_type = "Fudge"
        die_range = FUDGE_RANGE
    elif "w" == sides:
        roll_type = "WoD"
        lowest_success = 8
        die_range = range(1, 11) # WoD dice are ten-sided.
    elif "wc" == sides:
        if dice > 1:
           self.msg(origin.sender, "DICE: %s, only one WoD Chance die can be rolled." % (origin.nick))
           return
        roll_type = "WoD-Chance"
        lowest_success = 10
        die_range = range(1, 11)
    elif "p" == sides:
        roll_type = "Pool"
        die_range = range(1, 7)
    else:
        roll_type = "Standard"
        sides = int(sides)
        if sides > 1000:
            self.msg(origin.sender, "DICE: %s, the number of sides to the die is limited to 1000." % (origin.nick))
            return
        die_range = range(1, sides + 1)   

    result, die_result_string = (0, "")

    # Because of rerolls, this must be done differently.
    if roll_type == "WoD" or roll_type == "WoD-Chance":
        dice_to_roll = dice
        in_rerolls = False
        while dice_to_roll > 0:
            x = random.choice(die_range)
            if x >= lowest_success:
                result += 1
                x_str = "*%s*" % str(x)
            else:
                x_str = str(x)
            if len(die_result_string) == 0:
                die_result_string += x_str
            elif in_rerolls:
                die_result_string += " -> " + x_str
            else:
                die_result_string += ", " + x_str
            if x == 10: # Reroll.  Print differently for the next number.
                in_rerolls = True
            else:
                in_rerolls = False
                dice_to_roll -= 1

        # WoD Chance Rolls cause a 'Dramatic Failure' on the roll of a 1.
        if roll_type == "WoD-Chance" and result == 0 and x == 1:
            dramatic_failure = 1
        else:
            dramatic_failure = 0
    else:
        for i in xrange(dice):
            x = random.choice(die_range)
            x_str = str(x)
            if roll_type == "Pool":
                if x == 1:
                    result += 1
                    x_str = "*%s*" % x_str
            else:
               result += x
            if roll_type == "Fudge":
                if x == -1:
                    x_str = "-"
                elif x == 0:
                    x_str = "o"
                else:
                    x_str = "+"
            if len(die_result_string) == 0:
                die_result_string += x_str
            else:
                die_result_string += ", " + x_str
            
    if roll_type == "Fudge":
        self.msg(origin.sender, "DICE: %s rolled %sdF (Fudge); the result was %s. [ %s ]" % (origin.nick, dice, result, die_result_string))
    elif roll_type == "WoD":
        self.msg(origin.sender, "DICE: %s rolled %sdW (WoD); the success count was %s. [ %s ]" % (origin.nick, dice, result, die_result_string))
    elif roll_type == "WoD-Chance":
        if dramatic_failure:
            self.msg(origin.sender, "DICE: %s made a WoD Chance Roll; they had a Dramatic Failure. [ %s ]" % (origin.nick, die_result_string))
        else:
            self.msg(origin.sender, "DICE: %s made a WoD Chance Roll; the success count was %s. [ %s ]" % (origin.nick, result, die_result_string))
    elif roll_type == "Pool":
        if result > 0:
            result_txt = "Success!"
        else:
            result_txt = "Failure!"
        self.msg(origin.sender, "DICE: %s rolled %sdP (Pool); %s [ %s ]" % (origin.nick, dice, result_txt, die_result_string))
    else: # Standard dice.
        self.msg(origin.sender, "DICE: %s rolled %sd%s; the result was %s. [ %s ]" % (origin.nick, dice, sides, result, die_result_string))
    
f_roll.rule = (['roll'], r'([0-9]+)[Dd]([0-9]+|[fF]|[wW][cC]|[wW]|[pP])')
FUDGE_RANGE = (-1, -1, 0, 0, 1, 1)

@Energetic (40, 25)
def f_wp(self, origin, match, args):
    """*Wikipedia link*, Usage: ``!wp <topic>``

- *!wp <topic>*       Creates a URL/hyperlink for the page <topic> on Wikipedia.

"""
    page = urllib.quote(match.group(1).replace(" ", "_"))
    self.msg(origin.sender, "WIKIPEDIA: http://en.wikipedia.org/wiki/%s" % page)
    
f_wp.rule = (['wp'], r'(.*)')

@Energetic (50, 25)
@Channel_Only ('WIKI')
def f_wiki(self, origin, match, args):
    """*Custom Wiki link*, Usage: ``!wiki <topic>``

- *!wiki <topic>*       Creates a URL/hyperlink for the page <topic> on the channel's custom Wiki.

"""
    # Due to the fact that a custom Wiki may not be set up, we cheat.  We
    # attempt to print it, and assume that if it doesn't work, it's because
    # a custom Wiki is not defined.
    try:
        page = urllib.quote(match.group(1).replace(" ", "_"))
        self.msg(origin.sender, "WIKI: %s/%s" % (config.wikis[origin.sender], page))
    except:
        self.msg(origin.sender, "WIKI: Custom wiki not defined.")

f_wiki.rule = (['wiki'], r'(.*)')

@PM_Only
def f_testorigin(self, origin, match, args):
    self.msg(origin.sender, "TESTORIGIN: Nick = %s, User = %s, Host = %s, Sender = %s" % (repr(origin.nick), repr (origin.user), repr (origin.host), repr (origin.sender)))

f_testorigin.rule = (['testorigin'], r'(.*)')

new_word_responses = ['And behind Door Number Three, %s finds ... a new "%s"!',
                      'Dear %s: My love is like a red, red "%s".',
                      'Once upon a time, %s came upon a magic "%s".',
                      'There are only three sure things in %s\'s life: death, taxes, and plentiful "%s".',
                      'The rain in %s\'s mental landscape falls mainly on the "%s".',
                      'Psst.  I heard that %s was doing ... unspeakable things ... with a "%s" yesterday!',
                      'Able was %s ere they saw "%s"',
                      'Betty and %s bought some butter, but, they said, the butter\'s "%s"!',
                      '%s is trying to get the new hit song "%s" stuck in your head.  Run!',
                      'Alas, poor %s, I knew "%s" well!',
                     ]

@Energetic (75, 25)
def f_newword(self, origin, match, args):
    """*Invent a new word*, Usage: ``!newword #``

- *!newword*        Invents a new word that is roughly # characters long.

"""
    wordlen = int(match.group(1))
    if wordlen < 2 or wordlen > 20 and wordlen != 42:
       self.msg(origin.sender, "NEWWORD: %s, the length must be an integer between 2 and 20." % (origin.nick))
    else:
       word = wordgen.new_word(wordlen)
       response_string = random.choice(new_word_responses) % (origin.nick, word)
       self.msg(origin.sender, "NEWWORD: %s" % (response_string))

f_newword.rule = (['newword'], r'([0-9]+)')
