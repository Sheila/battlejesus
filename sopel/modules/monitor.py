#!/usr/bin/env python
"""monitor.py - monitor for kicks and mode changes in #nanowrimo then report to #nanowrimo-ops."""
from hooking import add_hook
import datetime

hook_report_channel = "#nanowrimo-ops"

def kick_hook (self, args, origin, text, line):
    if args[1].lower() in ("#nanowrimo", "#nanohaven", "#elsewhere"):
        msg = text
        self.msg(hook_report_channel, "MONITOR: %s kicked %s from %s at %s with message \"%s\"." % (origin.nick, args[2].strip(), args[1].strip(), datetime.datetime.now(), msg))

def mode_hook (self, args, origin, text, line):
    if args[1].lower() in ("#nanowrimo", "#nanohaven", "#elsewhere"):
        try:
            result = ' '.join(args[2:]).strip()
        except:
            self.msg(hook_report_channel, "MONITOR: %s set mode %s in %s at %s." % (origin.nick, args[2].strip(), args[1].strip(), datetime.datetime.now()))
        else:
            self.msg(hook_report_channel, "MONITOR: %s set mode %s in %s at %s." % (origin.nick, result, args[1].strip(), datetime.datetime.now()))

add_hook('KICK', kick_hook)
add_hook('MODE', mode_hook)
