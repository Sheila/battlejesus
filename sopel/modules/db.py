#!/usr/bin/env python
"""
wordwar_db.py - Database functions
Author: Me
About: http://jesus.thewrigro.com
"""

import os
import time
from sqlite3 import dbapi2 as sqlite

import ircmatch

from sopel.module import commands

def checkflags(bot, origin, flag):
    wdb = bot.db
    wdb.execute('SELECT user, flags FROM flagged_users ORDER BY user_id;')
    rows = wdb.fetchall()

    for row in rows:
        if ircmatch.match(0, row['user'], origin):
            if flag in row['flags'] or '*' in row['flags']:
                return True
            else:
                return False

    return False

