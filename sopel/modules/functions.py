
from functools import wraps

class Energy:
    def __init__(self, func, energy1, energy2):
        self._func = func
        self._e1 = energy1
        self._e2 = energy2

    def __call__(self, bot, trigger):
        import sopel.modules.energy as energy
        if energy.attempt_energetic_command(trigger.hostmask, self._e1, self._e2):
            self._func(bot, trigger)

def energetic(energy1, energy2):
    @wraps
    def spend(func):
        return Energy(func, energy1, energy2)
    return spend

