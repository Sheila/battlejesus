
from sopel.module import commands

@commands('test')
def test(bot, trigger):
    if trigger.nick != 'Aerdan':
        return

    channel = trigger.match.group(3)
    text = trigger.args[1].replace(trigger.match.group(0) + ' ', '').replace(channel + ' ', '')
    bot.say(text, trigger.sender)

