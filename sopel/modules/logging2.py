
import re
import time

from collections import namedtuple

import ircmatch

from sopel.module import commands, require_privmsg

from sopel.modules.flags import admin_only

def setup(bot):
    bot.db.execute('''
        CREATE TABLE IF NOT EXISTS logging (
            id INTEGER PRIMARY KEY,
            user VARCHAR(32) NOT NULL DEFAULT '',
            touch BIGINT(32) NOT NULL DEFAULT 0,
            message VARCHAR(400) NOT NULL DEFAULT '',
            logger VARCHAR(32) NOT NULL DEFAULT ''
        );
    ''')

@admin_only
@require_privmsg
@commands('l', 'log')
def log(bot, trigger):
    user, text = trigger.group(2).split(' ', maxsplit=1)

    cursor = bot.db.execute(
        'INSERT INTO logging (user, touch, message, logger) VALUES (?, ?, ?, ?)',
        user, time.time(), text, trigger.sender
    )
    if cursor.rowcount < 1:
        bot.say('LOGGING: Unable to log this note, sorry. Contact Aerdan for assistance.', trigger.sender)
    else:
        bot.say('LOGGING: Note logged for {}.'.format(user), trigger.sender)

@admin_only
@require_privmsg
@commands('t', 'last')
def last(bot, trigger):
    user = trigger.group(3)
    idx = trigger.group(4)

    try:
        idx = int(idx)
    except TypeError:
        idx = -1
    except ValueError:
        return

    cursor = bot.db.execute('SELECT user, touch, message, logger FROM logging WHERE user LIKE ? ORDER BY touch', user)
    rows = cursor.fetchall()
    if len(rows) == 0:
        bot.say('LOGGING: Found no notes regarding {}.'.format(user), trigger.sender)
    else:
        try:
            row = rows[idx]
        except IndexError:
            bot.say('LOGGING: Index {} is out of bounds; highest index is {}'.format(idx, len(rows)), trigger.sender)
            return
        bot.say(
            'LOG [{}/{}]: "{}" (For {}, by {}, {}'.format(
                idx if idx > 0 else len(rows) + idx,
                len(rows),
                row['message'],
                row['user'],
                row['logger'],
                datetime.datetime.fromtimestamp(int(row['touch'])).strftime('%a %F %H:%M:%S')
            )
        )
