#!/usr/bin/env python
"""
login.py - Login functions for the Word War Bot
    - !login
      !register
      !quote
      !count
      !password
Author: Me
About: http://jesus.thewrigro.com
"""

import db, time, re, energy

from flags import AdminOnly
from functions import *

def password (password):
    nr = 1345345333L
    add = 7
    nr2 = 0x12345671L
    tmp = ''
    total = ''
    for letter in password:
        if letter == ' ' or letter == '\t':
            pass
        else:
            tmp = long(ord(letter))
            nr ^= (((nr & 63) + add) * tmp) + (nr << 8)
            nr2 += (nr2 << 8) ^ nr
            add += tmp
    return str(str(hex(nr & ((1L << 31) -1L))).replace("x", "").replace("L", "") + str(hex(nr2 & ((1L << 31) -1L))).replace("0x", "").replace("L", "")).lower()

def logincheck(origin):
    # Checks to see if a user is logged in
    # compares origin.nick, origin.user (ident/gecos)
    # and origin.host (hostmask) with login 
    # table.
    wdb = db.getcursor()
    wdb.execute("SELECT * FROM login WHERE nick = ? LIMIT 1", (origin.nick,))
    rows = wdb.fetchall()
    result = False
    if len(rows) >= 1:
        # Found rows:
        for row in rows:
            if row['ident'] == origin.user and row['host'] == origin.host and row['expire'] >= time.time():
                # Found a login string
                wdb.execute("""UPDATE login SET expire = ? WHERE ident = ? AND host = ?""", (time.time() + 3600, row['ident'], row['host']))
                result = row
    db.closecursor(wdb)
    return result
        

### Block of functions allowing login/logout etc ###

@PM_Only
def f_register(self, origin, match, args):
    """*Registration*, Usage: ``!register <password>``

- *!register <password>*      Register with the bot using <password> as your password. Your username will correspond to your current nickname. If the current nickname is already registered, you won't be able to register it a second time. If you forget your password, contact mat.

"""
    wdb = db.getcursor()
    if match.group(1) != "":
        wdb.execute("SELECT `id` FROM users WHERE `name`= ? LIMIT 1", (origin.nick, ))
        row = wdb.fetchone()
        if row:
            self.msg(origin.nick, "WORD WAR: The username '%s' has already been registered. Please choose another one." % origin.nick);
        else:
            wdb.execute("INSERT INTO users VALUES (NULL, ?, NULL, ?, 0)", (origin.nick, password(match.group(1))))
            user_id = db.insert_id(wdb)
            wdb.execute("INSERT INTO login VALUES (?, ?, ?, ?, ?, ?, ?)", (user_id, origin.nick, origin.nick, origin.user, origin.host, time.time(), time.time() + 3600))
            self.msg(origin.nick, "WORD WAR: You are now registered! You have also been logged in. Your session will expire in 1 hour. Your password is '%s', please record it somewhere. If you forget or lose this password, contact mat to have it reset to a new one. To login in future, type '!login <password>' in private message." % match.group(1))
    else:
        self.msg(origin.nick, "WORD WAR: Invalid password. The format is '!register <password>'.")
        
    db.closecursor(wdb)

f_register.rule = (['register'], r'(.*)')

@PM_Only
def f_login(self, origin, match, args):
    """*Login*, Usage: ``!login <password>``

- *!login <password>*      Login with the bot; login sessions last 60 minutes, but will expire earlier with !logout.

"""
    wdb = db.getcursor()
    wdb.execute("SELECT `id`, password FROM users WHERE `name`= ? LIMIT 1", (origin.nick,))
    row = wdb.fetchall()
    if len(row) >= 1:
        row = row[0]
        if row['password'] == password(match.group(1)):
            # matching password
            wdb.execute("DELETE FROM login WHERE `user_id`= ?", (row['id'],))
            wdb.execute("INSERT INTO login VALUES (?, ?, ?, ?, ?, ?, ?)", (row['id'], origin.nick, origin.nick, origin.user, origin.host, time.time(), time.time() + 3600))
            self.msg(origin.nick, "WORD WAR: You are now logged in. Your login will expire in 1 hour, or if you logout via the !logout function. You can reset this one hour period by logging in again.")            
        else:
            # incorrect password
            self.msg(origin.nick, "WORD WAR: Invalid password '%s'. Please try again." % match.group(1))
    else:
        self.msg(origin.nick, "WORD WAR: The username '%s' does not exist. Please register it using the format '!register <password>'." % origin.nick)    
    db.closecursor(wdb)

f_login.rule = (['login'], r'(.*)')

@Login_Only
@PM_Only
def f_logout(self, origin, match, args):
    """*Logout*, Usage: ``!logout``

- *!logout*       Expire your current login session.

"""
    wdb = db.getcursor()
    wdb.execute("DELETE FROM login WHERE nick = ?", (origin.nick,))
    db.closecursor(wdb)
    self.msg(origin.nick, "WORD WAR: You have been logged out.")

f_logout.rule = (['logout'], r'(.*)')

@Login_Only
@PM_Only
def f_password(self, origin, match, args):
    """*Change your password*, Usage: ``!password <password>``

- *!password <password>*       If logged in, will change your password to the text contained in <password>
"""
    wdb = db.getcursor()
    row = f_password.login_row
    print row
    wdb.execute("UPDATE users SET password=? WHERE id=?", (password(match.group(1)), row['user_id']))
    if wdb.rowcount == 1:
        # reset password:
        self.msg(origin.nick, "WORD WAR: Your password is now '%s'. Please contact mat if it doesn't work." % match.group(1))
    else:
         # couldn't reset password, error!
        self.msg(origin.nick, "WORD WAR: An error has occured. Please contact an admin if this continues.")
    db.closecursor(wdb)

f_password.rule = (['password'], r'(.*)')
### Block of logincheck functions ###

# cf. Willocwen:
#18:42:11 <mat> okay
#18:42:13 <mat> will fix
#18:42:24 <mat> i am writing an admin function so that i don't have to do it via
#               sql all the time :)
#18:42:25 <mat> ;)
@AdminOnly
def f_setcount(self, origin, match, args):
    """*Change a wordcount*, Usage: ``!setcount [name] [#]``

- *!setcount [name] [#]*       Change [name]'s wordcount to [#].

"""
    wdb = db.getcursor()
    user = match.group(1)
    count = match.group(2)
    wdb.execute("SELECT * FROM users WHERE name = ? LIMIT 1", (user,))
    row = wdb.fetchone()
    if row.has_key('id'):
        uid = row['id']
        # Get their current wordcount
        wdb.execute("UPDATE wordcounts SET count = ? WHERE user_id = ?", (count, uid))
        self.msg(origin.nick, "Reset %s's wordcount to %s." % (user, count))
    db.closecursor(wdb)

f_setcount.rule = (['setcount'], r'([^ ]*) ([0-9]+)')

def f_count(self, origin, match, args):
    """*Word counts*, Usage: ``!count [#|<username>|]``

- *!count #*       While logged in, adjust your word count by #. Negative numbers are currently disabled allowed. Positive is assumed by default. The maximum number you can increase your wordcount by is 5000.
- *!count <username>*       At any time, you can check the word count of a user <username> if they are registered and have updated their wordcount.
- *!count*       Check your current word count at any time.

"""
    row = logincheck(origin)
    wdb = db.getcursor()
    if match.group(1).isdigit():
        # numeric, someone trying to inc their count
        if row:
            try:
                digit = int(re.compile("([-0-9]+)").match(match.group(1) or '').group(1))
            except:
                digit = 0
            if digit <= 5000:
                # okay, yay!

                # Get the old count.
                wdb.execute("SELECT count FROM wordcounts WHERE user_id = ? LIMIT 1", (row['user_id'],))
                count_row = wdb.fetchone() or {'count': 0}
                if count_row.has_key("count"):
                    curr_count = int(count_row["count"])
                else:
                    curr_count = 0

                new_count = curr_count + digit
                # Update the count.
                wdb.execute("INSERT OR REPLACE INTO wordcounts VALUES (?, ?, 0);", (row['user_id'], new_count))
                self.msg(origin.nick, "WORD WAR: Your count has been updated by %s words. To view your total wordcount, type !count." % digit)
            else:
                self.msg(origin.nick, "WORD WAR: %s exceeds the !count limit of 5000. Please try again with a smaller number." % digit)
        else:
            self.msg(origin.nick, "WORD WAR: You are not currently logged in. If you believe that you are and there is an error, or you are unable to log in, please contact Rowan or Aerdan.")
    elif match.group(1) == '':
        # null, someone wants their own count
        wdb.execute("SELECT count FROM wordcounts JOIN users ON wordcounts.user_id=users.id WHERE users.name=? LIMIt 1", (origin.nick,))
        row = wdb.fetchone() or {}
        if row.has_key('count'):
            if row['count'] == None:
                self.msg(origin.nick, "WORD WAR: You are not registered, or you have not updated your count.")
            else:
                self.msg(origin.nick, "WORD WAR: Your total, current word count is: %s." % row['count'])
        else:
            self.msg(origin.nick, "WORD WAR: You are not registered, or you have not updated your count.")
    else:
        # someone wants someone else's count
        wdb.execute("SELECT count FROM wordcounts JOIN users ON wordcounts.user_id=users.id WHERE users.name=? LIMIT 1", (match.group(1),))
        row = wdb.fetchone() or {}
        if row.has_key('count'):
            if row['count'] == None:
                self.msg(origin.nick, "WORD WAR: %s is not registered, or has not updated their count." % match.group(1))
            else:
                self.msg(origin.nick, "WORD WAR: %s's total, current word count is: %s." % (match.group(1).strip(), row['count']))
        else:
            self.msg(origin.nick, "WORD WAR: %s is not registered, or has not updated their count." % match.group(1))
    db.closecursor(wdb)

f_count.rule = (['count'], r'(.*)')

