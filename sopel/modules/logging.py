#!/usr/bin/env python
""" logging.py - a module to log chat channels """
import rconfig
config = rconfig.config
import os, time, re

logdir = config.datadir + 'logs/'
logappend = '%F'
if not os.path.exists(logdir):
    os.mkdir(logdir)

logfiles = {}

whereisdat = {}

def whereis (username):
    if whereisdat.has_key(username):
        return whereisdat[username]
    else:
        return False
        
def foundin (username, channel):
    if whereisdat.has_key(username):
        if channel not in whereisdat[username]:
            whereisdat[username].append(channel)
    else:
        whereisdat[username] = [channel, ]
        
def butleft (username, channel):
    if whereisdat.has_key(username):
        if channel in whereisdat[username]:
            del whereisdat[username][whereisdat[username].index(channel)]
            
def butquit (username):
    if whereisdat.has_key(username):
        del whereisdat[username]
        
def andisnow (username, newname):
    if whereisdat.has_key(username):
        whereisdat[newname] = whereisdat[username]

def get_logfile (channel):  
    """ 
        We associate specific files with specific channels. Assume that "channel" is either the channel that the message originated in, or the username (for private messaging).
    """
    if logfiles.has_key(channel):
        timestamp, logfile = logfiles[channel]
        if timestamp == time.strftime(logappend):
            return logfile
        logfile.close()
        print "Closed %s-%s.log" % (channel, timestamp)
    logfile = open(logdir+channel+'-'+time.strftime(logappend)+'.log', 'a')
    logfiles[channel] = (time.strftime(logappend), logfile)
    print "Opened %s-%s.log" % (channel, time.strftime(logappend))
    return logfile

def do_log (sender, text):
    logfile = get_logfile(sender)
    logfile.write(text + '\n')
    logfile.flush()
    
def parse_me (channel, text, notice=False):
    msg = "%s " % time.strftime("%H:%M:%S")
    if notice:
        msg += '-%s- %s' % (config.nick, text)
    else:
        msg += '<%s> %s' % (config.nick, text)
    do_log(channel, msg)
    
def parse_log (text):
    style = {'join': '-!- %s [%s@%s] has joined %s', # $
'part': '-!- %s [%s@%s] has left %s [%s]', # 5
'quit': '-!- %s [%s@%s] has quit [%s]', # 4
'nick': '-!- %s is now known as %s', # 2
'kick': '-!- %s was kicked from %s by %s [%s]', # 4
'mode': '-!- mode/%s [%s] by %s', # 3
'lusers': "[%s] Users: %s", # 2
'lusers2': "[%s] End of /names", # 1
'privmsg': '<%s> %s', # 2
'chanmsg': '<%s> %s', # 3
'privctcp': '%s [%s@%s] requested CTCP %s', # 4
'chanctcp': '%s [%s@%s] requested CTCP from %s: %s', # 5
'privnotice': '-%s(%s@%s)- %s', # 4
'channotice': '-%s:%s- %s', # 4
'privaction': '* %s %s', # 2
'chanaction': '* %s %s', # 3
'topic': 'Topic for %s: %s', # 2
'topicsetby': 'Topic set by %s [] [%s]', # 2
'topicchange': '%s changed the topic of %s to: %s', # 3
'self_priv_msg': '[privmsg: %s] <%s> %s', # 3
'self_priv_notice': '[notice(%s)] %s', # 2
'info': '>> %s', # 1
'base': "-!- %s", # 1
}
    if text.startswith(":"):
        donelog = False
        channel = False
        origin, text = text.split(" ", 1)
        
        orfind = re.compile(r':([^!]*)!?([^@]*)@?(.*)')
        if orfind.match(origin):    
            nick, gecko, hostmask = orfind.match(origin).groups()
        else:
            nick = origin.lstrip(':')
            gecko = ''
            hostmask = nick

        origin = {"nick": nick.strip(), "gecko": gecko.strip(), "hostmask": hostmask.strip()}
        numeric, data = text.split(" ", 1)
        msg = "%s " % time.strftime("%H:%M:%S")
        if numeric == 'JOIN':
            channel = data.replace(":", "")
            msg += style['join'] % (origin['nick'], origin['gecko'], origin['hostmask'], data.replace(":", ""))
            foundin(origin['nick'], channel)
        elif numeric == 'PART':
            if ":" in data:
                channel, message = data.split(" :", 1)
            else:
                channel = data
                message = ""
            butleft(origin['nick'], channel)
            msg += style['part'] % (origin['nick'], origin['gecko'], origin['hostmask'], channel.strip(), message.strip())
        elif numeric == 'QUIT':
            msg += style['quit'] % (origin['nick'], origin['gecko'], origin['hostmask'], data.replace(":Quit:", "Quit:"))
            chans = whereis(origin['nick'])
            if chans:
                donelog = True
                for chan in chans:
                    do_log(chan, msg)    
            butquit(origin['nick'])
        elif numeric == 'NICK':            
            msg += style['nick'] % (origin['nick'], data.replace(":", ""))
            chans = whereis(origin['nick'])
            if chans:
                donelog = True
                for chan in chans:
                    do_log(chan, msg)
            andisnow(origin['nick'], data.replace(":", ""))
        elif numeric == 'KICK':
            channel, user, message = data.split(" ", 2)
            msg += style['kick'] % (user, channel, origin['nick'], message[1:len(message)])
        elif numeric == 'TOPIC':
            channel, message = data.split(' :')
            msg += style['topicchange'] % (origin['nick'], channel, message)
        elif numeric == 'MODE':
            channel, modestring = data.split(" ", 1)
            if modestring.startswith(":"):
                modestring = modestring[1:len(modestring)]
            msg += style['mode'] % (channel, modestring, origin['nick'])
        elif numeric == 'PRIVMSG':
            target, text = data.split(" ", 1)
            channel = target
            text = text[1:len(text)].strip()
            if text.startswith(""):
                text = text.replace("ACTION", "")
                text = text.replace("", "")
                text = text.strip()
                if target.startswith("#"):
                    msg += style['chanaction'] % (origin['nick'], text)
                else:
                    channel = origin['nick']
                    msg += style['privaction'] % (origin['nick'], text)
            else:
                if target.startswith("#"):
                    msg += style['chanmsg'] % (origin['nick'], text)
                else:
                    channel = origin['nick']
                    msg += style['privmsg'] % (origin['nick'], text)
        elif numeric == 'NOTICE':
            target, text = data.split(" ", 1)
            text = text[1:len(text)].strip()
            if target.startswith("#"):
                msg += style['channotice'] % (origin['nick'], target, text)
            else:
                msg += style['privnotice'] % (origin['nick'], origin['gecko'], origin['hostmask'], text)
        elif numeric == '332' or numeric == '331':
            myself, channel, topic = data.split(" ", 2)
            msg += style['topic'] % (channel, topic[1:len(topic)].strip())
        elif numeric == '333':
            myself, channel, user, timestamp = data.split(" ", 3)
            msg += style['topicsetby'] % (user, time.strftime("%a %b %H:%M:%S %Y", time.gmtime(int(timestamp))))
        elif numeric == '375' or numeric == '372' or numeric == '376':
            myself, text = data.split(" ", 1)
            msg += style['base'] % text[1:len(text)].strip()
        elif numeric == '353':
            myself, atsign, channel, users = data.split(" ", 3)
            usersd = [x.strip('~&@+%') for x in users[1:len(users)].strip().split()]
            for user in usersd:
                foundin(user, channel)
            msg += style['lusers'] % (channel, users[1:len(users)].strip())
        elif numeric == '366':
            myself, channel, eof = data.split(" ", 2)
            msg += style['lusers2'] % channel
        else:
            msg += "Unknown numeric %s: %s %s" % (numeric, origin, data)
        if msg != '': # and options.verbose:
            final_text = re.sub("([^]*)", chr(27)+"[1;37m\g<1>"+chr(27)+"[0m", msg)
            if not donelog:
                if not channel:
                    channel = origin['nick']
                do_log(channel, final_text)                           
