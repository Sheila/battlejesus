#!/usr/bin/env python
"""
   various utility functions that aren't exposed to the user.

   written by Phil Bordelon.
"""

def minute_str(length):
   """Parameters:
   length: length of time

   Returns the appropriate string representing a number of minutes.
   """

   # First, get rid of annoying ".0"s when it's an exact amount.
   if int(length) == length:
      string = "%d" % int(length)
   else:
      string = "%s" % repr(length)

   # Next, handle minute vs. minutes.
   string += " minute"
   if length != 1:
      string += "s"

   return string
