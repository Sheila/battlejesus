#!/usr/bin/env python
"""
For interfacing with the word count API
"""

import urllib, re
from xml.dom import minidom

from sopel.module import commands

def get (href):
    try:
        url = urllib.urlopen(href)
        data = url.read()
        url.close
    except:
        data = ""
    return data

@commands('api')
def api(bot, trigger):
    """*Access the NaNoWriMo API*, Usage: ``!api [u|r]count [#]``

- *!api ucount <#>*       Display the current word count for user number <#>.
- *!api rcount <#>*       Display the current word count for region number <#>.

"""
    # options:
    # ucount #
    # rcount #
    if trigger.match.group(3) is None or trigger.group.match(4) is None:
        return
    count = re.compile(r'(ucount|rcount)\s*([0-9]+)').match(trigger.match.group(3))
    base_url = "http://www.nanowrimo.org/modules/wcapi/"
    base_index = {"ucount": "wc.php?uid=", "rcount": "wcregion.php?rid="}
    if count:
        target = trigger.match.group(3)
        uid = trigger.match.group(4)
        xml_data = minidom.parseString(get("%s%s%s" % (base_url, base_index[target], uid)))
        try:
            if target == 'ucount':
                count = xml_data.getElementsByTagName('user_wordcount')[0].firstChild.data
                name = xml_data.getElementsByTagName('uname')[0].firstChild.data
            elif target == 'rcount':
                count = xml_data.getElementsByTagName('region_wordcount')[0].firstChild.data
                name = xml_data.getElementsByTagName('rname')[0].firstChild.data
        except IndexError:
            count = None
            name = None
        if count is not None:
            bot.say('API: Word count for %s is %s.' % (name, count), trigger.nick)
        else:
            bot.say('API: Invalid id: %s.' % (uid,), trigger.nick)
        # wc.php?uid= 
        # wcregion.php?rid=

