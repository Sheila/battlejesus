
import re
import time

from sopel.module import commands, require_chanmsg, require_privmsg

import sopel.modules.energy as energy

from sopel.modules.flags import checkflags, admin_only
from sopel.modules.functions import energetic

@commands('cqhelp', 'cquotehelp')
def help(bot, trigger):
    bot.say('QUOTE: The format for adding a quote is "!cqadd (text of the quote)". The channel will be informed that the quote has been added correctly, along with the number of the quote. To view a specific quote that you know the number of, the format is "!cq (number here)". To view a random quote, along with its number, the format is "!cq". To view the total amount of quotes in the database, the format is "!cqcount".')

@energetic(100, 20)
@require_chanmsg
@commands('cqadd', 'cquoteadd')
def add(bot, trigger):
    wdb = bot.db
    channel = trigger.sender
    quote = ' '.join(trigger.args)
    cursor = wdb.execute('SELECT cur_count AS maxint FROM cquote_control WHERE channel=?', channel)
    row = cursor.fetchone()

    if row is not None:
        if row[0] is None:
            index = 0
        else:
            index = int(row[0])+1
        cursor = wdb.execute("INSERT INTO `cquote` VALUES (NULL, 1, ?, ?, ?, ?, ?)", (channel, index, quote, time.time(), origin.nick))
        if cursor.rowcount < 1:
            # Didn't succeed
            bot.say("QUOTE: Quote failed. Please try again, or contact Aerdan.", trigger.sender)
        else:
            # Yay!
            bot.say("QUOTE: Quote added! Quote is #%s." % (index,), trigger.sender)
            wdb.execute("UPDATE `cquote_control` SET cur_count = ? WHERE channel=?", (index, channel))
    else:
        bot.say("QUOTE: An error occured. Please contact Aerdan.", trigger.sender)

@energetic(100, 100)
@require_chanmsg
@commands('cqcount', 'cquotecount')
def count(bot, trigger):
    wdb = bot.db
    channel = trigger.sender.lower()
    cursor = wdb.execute("SELECT COUNT(*) AS total FROM `cquote` WHERE channel=? AND active=1", (channel, ))
    row = cursor.fetchone()
    if row is not None:
        bot.say("QUOTE: There is a total of %s quotes in the database." % (row[0],), trigger.sender)
    else:
        bot.say("QUOTE: An error has occured, please contact Aerdan.", trigger.sender)

@energetic(100, 20)
@require_chanmsg
@commands('cq', 'cquote')
def quote(bot, trigger):
    wdb = bot.db
    channel = trigger.sender.lower()
    if trigger.match.group(3) is None:
        cursor = wdb.execute("SELECT text, channel_index, nick FROM `cquote` WHERE channel=? AND active=1 ORDER BY RANDOM() LIMIT 1", (channel, ))
        row = cursor.fetchone()
        if row is not None:
            bot.say("RANDOM QUOTE: %s (#%s, added by %s)" % (row[0].replace('\\', '').replace("''", "'"), row[1], row[2]), trigger.sender)
        else:
            bot.say("QUOTE: An error occured. Please contact Aerdan.", trigger.sender)
    elif trigger.match.group(3).isdigit():
        chan_index = int(trigger.match.group(3))
        cursor = wdb.execute("SELECT channel_index, text, nick FROM `cquote` WHERE channel=? AND active=1 AND channel_index=? LIMIT 1", (channel, chan_index))
        row = cursor.fetchone()
        if row is not None:
            bot.say("QUOTE #%s: %s (added by %s)" % (row[0], row[1].replace('\\', '').replace("''", "'"), row[2]), trigger.sender)
        else:
            bot.say("QUOTE: Quote #%s does not exist." % (trigger.match.group(3),), trigger.sender)

@admin_only
@require_privmsg
@commands('cqkill', 'cquotekill')
def kill(bot, trigger):
    wdb = bot.db
    if trigger.match.group(4).isdigit():
        quote = trigger.match.group(4)
        channel = trigger.match.group(3).lower()
    else:
        bot.say("QUOTE: Invalid parameters.", trigger.sender)

    wdb.execute("UPDATE `cquote` SET active=0 WHERE channel=? AND channel_index=?", channel, quote)
    if wdb.rowcount < 1:
        bot.say("QUOTE: Unable to delete quote {}#{}; it might not exist.".format(channel, quote), trigger.sender)
    else:
        bot.say("QUOTE: Deleted quote {}#{}.".format(channel, quote), trigger.sender)
