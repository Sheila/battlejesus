#!/usr/bin/env python
"""whois.py - keep track of who is where!

USER_DATA_ARRAY = {
    "channel":
        [ username1, username2, username3, username3, ... usernameX ],
    "channel2":
        ...
    }

User data is manipulated by the functions add_user_to_channel, del_user_from_channel, and change_username.

These functions are called whenever a whois is performed on a user, users join, part, quit, or are kicked from a channel, or change their nickname. The entire data for a channel is dumped and refreshed on names.

Data can be accessed externally from the program by using the functions whois and user_in_channel. 

"""
import re, time
from hooking import add_hook
from flags import AdminOnly
from functions import *

USER_DATA_ARRAY = {}
WHOIS_RESPONSE = []

# Private, hook-based functions using the hooking module.
def hook_whois (self, args, origin, text, line):
    global WHOIS_RESPONSE
    username = args[2]
    stripper = re.compile('[^A-Za-z0-9#]')
    channels = [re.subn(stripper, '', x)[0] for x in text.split()]
    WHOIS_RESPONSE.append((username, channels))
    print WHOIS_RESPONSE
    for channel in channels:
        add_user_to_channel(username, channel)

def hook_nick (self, args, origin, text, line):
    change_username(origin.nick, text)

def hook_join (self, args, origin, text, line):
    add_user_to_channel(origin.nick, origin.sender)

def hook_part (self, args, origin, text, line):
    del_user_from_channel(origin.nick, origin.sender)

def hook_quit (self, args, origin, text, line):
    del_user_globally(origin.nick)

def hook_kick (self, args, origin, text, line):
    del_user_from_channel(args[2], args[1])

def hook_names (self, args, origin, text, line):
    stripper = re.compile('[^A-Za-z0-9\[\]\\\^_-\{\|\}]')
    channel = args[3]
    users = [re.subn(stripper, '', x)[0] for x in text.split()]
    for user in users:
        add_user_to_channel(user, channel)

add_hook('319', hook_whois)
add_hook('353', hook_names)
add_hook('NICK', hook_nick)
add_hook('JOIN', hook_join)
add_hook('PART', hook_part)
add_hook('QUIT', hook_quit)
add_hook('KICK', hook_kick)

# Private data manipulation functions.
def add_user_to_channel (username, channel):
    global USER_DATA_ARRAY
    if username:
        username = username.lower()
        if channel:
            channel = channel.lower()
            if not USER_DATA_ARRAY.has_key(channel):
                USER_DATA_ARRAY[channel] = []

            if username not in USER_DATA_ARRAY[channel]:
                USER_DATA_ARRAY[channel].append(username)

    return True

def del_user_from_channel (username, channel):
    global USER_DATA_ARRAY
    username = username.lower()
    channel = channel.lower()
    if not USER_DATA_ARRAY.has_key(channel):
        return True
    else:
        USER_DATA_ARRAY[channel] = [x for x in USER_DATA_ARRAY[channel] if x != username]
        return True

def del_user_globally (username):
    global USER_DATA_ARRAY
    username = username.lower()
    for channel in USER_DATA_ARRAY.keys():
        if user_in_channel(username, channel):
            del_user_from_channel(username, channel)

def change_username (old_username, new_username):
    global USER_DATA_ARRAY
    for channel in USER_DATA_ARRAY.keys():
        if user_in_channel(old_username, channel):
            del_user_from_channel(old_username, channel)
            add_user_to_channel(new_username, channel)

# Public functions.
def user_in_channel (username, channel):
    global USER_DATA_ARRAY
    if not USER_DATA_ARRAY.has_key(channel):
        return False
    return username.lower() in USER_DATA_ARRAY[channel]

def user_channels (username):
    global USER_DATA_ARRAY
    return [channel for channel, data in USER_DATA_ARRAY.iteritems() if username.lower() in data]
    
# User-interactive functions
@AdminOnly
def f_names (self, origin, match, args):
    """*Channel users*, Usage: ``!names [channel]``

- *!names <channel>*       Send a NAMES request to the server.

"""
    self.write(('NAMES', match.group(1)))
f_names.rule = (['names'], r'(#.*)')

@AdminOnly
def f_refresh_channels (self, origin, match, args):
    """*Channel users*, Usage: ``!rchan``

- *!rchan*       Send a NAMES request to the server for all currently joined channels.

"""
    import rconfig
    config = rconfig.config
    for channel in config.channels_in:
        self.write(('NAMES', channel))
f_refresh_channels.rule = (['rchan'], r'(.*)')

@AdminOnly
def f_print_data (self, origin, match, args):
    """*Channel users*, Usage: ``!pudata``

- *!pudata*       Output current USER_DATA_ARRAY to screen.

"""
    global USER_DATA_ARRAY
    print USER_DATA_ARRAY
f_print_data.rule=(['pudata'], r'(.*)')
