#!/usr/bin/env python
"""hook.py - hook functions to specific numerics.

perform_hooks is called every time found_terminator is called in irc.py's botobj.

It iterators over hooks, a dictionary, and calls functions with self (botobj), args, origin, text, and the original buffer from irc.py.

"""
import config, datetime
from functions import *

hooks = {}

def perform_hooks (self, args, origin, text, line):
    """Parameters:
        self = irc.py botobj
        args = irc.py:found_terminator's args
        origin = irc.py:found_terminator's origin
        text = irc.py:found_terminator's text line
        line = irc.py:found_terminator's orig_line
    """
    if hooks.has_key('*'):
        if isinstance(hooks['*'], list):
            for func in hooks['*']:
                try:
                    print "Attempting to run function %s for numeric %s." % (func, '*')
                    func(self, args, origin, text, line)
                except Exception, e:
                    self.msg(config.control_channel, "Error in function %s (%s)." % (func, e))

    if hooks.has_key(args[0]):
        if isinstance(hooks[args[0]], list):
            for func in hooks[args[0]]:
                try:
                    print "Attempting to run function %s for numeric %s." % (func, args[0])
                    func(self, args, origin, text, line)
                except Exception, e:
                    self.msg(config.control_channel, "Error in function %s (%s)." % (func, e))
        

def add_hook (numeric, function):
    global hooks
    if hooks.has_key(numeric):
        if isinstance(hooks[numeric], list):
            hooks[numeric].append(function)
    else:
        hooks[numeric] = []
        hooks[numeric].append(function)

