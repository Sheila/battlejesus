
import re
import operator
from functools import wraps

from sopel.module import commands
from sopel.modules.functions import energetic

import ircmatch

flagmap = {
    'a': 'View flags list: !flags list',
    'b': 'Ban from using BattleJesus.',
    'd': 'Access to debugging-related commands: !print-data, !print-buffer, !rehash, !reload, !join, !part, !servstat, !sqlreconn',
    'p': 'Manipulate prompt database: !prompt add, !prompt kill',
    't': 'Manipulate twist database: !twist add, !twist kill',
    '*': 'Godmode. Be extremely careful assigning this flag: all commands',
}
flagex = 'b'

def admin_only(func):
    @wraps
    def admin(bot, trigger):
        if checkflags(bot, trigger.hostmask, '*'):
            return func(bot, trigger)
        else:
            bot.say('You do not have permission to use this command.', trigger.nick)
    return admin

class Flag:
    def __init__(self, func, flag):
        self._func = func
        self._flag = flag

    def __call__(self, bot, trigger):
        if checkflags(bot, trigger.hostmask, flag):
            return func(bot, trigger)
        else:
            bot.say('You do not have permission to use this command.', trigger.nick)

def with_flag(flag):
    @wraps
    def check(func):
        return Flag(func, flag)
    return check

def _fetch_flags(bot, user):
    wdb = bot.db
    cursor = wdb.execute('SELECT user, flags FROM flagged_users ORDER BY user_id;')
    rows = cursor.fetchall()

    if rows is not None:
        matches = []

        for row in rows:
            if ircmatch.match(0, row[0], user):
                matches.append((row[0], row[1]))

        if len(matches) < 1:
            return 'FLAGS: No flags for {}'.format(user)

        user = max(matches, key=operator.itemgetter(0))
        return 'FLAGS: Flags for {} are: {}'.format(user[0], user[1])

    return 'FLAGS: No flags for {}.'.format(nick)

def _mod_flags(flags, base):
    adding = True

    for i in flags:
        if i == '+': # add flags
            adding = True
        elif i == '-':
            adding = False
        else:
            if i not in flagmap: # ignore invalid flags
                continue
            if adding and i not in base:
                base.append(i)
            elif not adding and i in base:
                base.remove(i)

    return ''.join(base)

def checkflags(bot, origin, flag):
    wdb = bot.db
    cursor = wdb.execute('SELECT user, flags FROM flagged_users ORDER BY user_id;')
    rows = cursor.fetchall()

    for row in rows:
        if ircmatch.match(0, row[0], origin):
            if flag in row[1] or (
                flag not in flagex and '*' in row[1]
            ):
                return True
            else:
                return False
    return False

@energetic(10, 75)
@commands('flags')
def flags(bot, trigger):
    wdb = bot.db

    manip_re = re.compile(r'([]a-zA-Z{|}*?`\[-][]a-zA-Z0-9{|}!@*?.`\[-]+) ([+-][a-zA-Z*]+){1,2}')
    show_re  = re.compile(r'([]a-zA-Z{|}*?`\[-][]a-zA-Z0-9{|}!@*?.`\[-]+)')

    if (
        trigger.match.group(3) is not None and
        trigger.match.group(4) is not None
        ):
        manip = manip_re.match(' '.join((trigger.match.group(3), trigger.match.group(4))))
    if trigger.match.group(3) is None:
        # show own flags
        bot.reply(_fetch_flags(bot, trigger.hostmask), trigger.sender)
    elif trigger.match.group(4) is not None and manip:
        if checkflags(trigger.hostmask, '*'):
            user = manip.group(1)
            flags = manip.group(2)

            cursor = wdb.execute('SELECT flags FROM flagged_users WHERE user=?;', [user])
            row = cursor.fetchone()

            if row is not None:
                flags = _mod_flags(flags, list(row[0]))
                wdb.execute('UPDATE flagged_users SET flags=? WHERE user=?;', [flags, user])
            else:
                flags = _mod_flags(flags, [])
                wdb.execute('INSERT INTO flagged_users (user, flags) VALUES (?, ?);', [user, flags])
    elif trigger.match.group(3) == 'list':
        if checkflags(bot, trigger.hostmask, 'a'):
            cursor = wdb.execute('SELECT user_id, user, flags FROM flagged_users ORDER BY user_id;')
            rows = cursor.fetchall()

            if len(rows) > 0:
                bot.say(' ID  {:20}  Flags'.format('Mask'), trigger.nick)
                for row in rows:
                    bot.say('{:3}  {:20}  {}'.format(row[0], row[1], row[2]), trigger.nick)
    elif trigger.match.group(3) == 'help':
        bot.say('Flag  Meaning', trigger.nick)

        keys = sorted(flagmap.keys())

        for key in keys:
            bot.say('{:4}  {}'.format(key, flagmap[key]), trigger.nick)
    elif trigger.match.group(3) == 'show':
        # show another user's flags
        if checkflags(bot, trigger.hostmask, 'a'):
            user = trigger.match.group(4)
            bot.say(_fetch_flags(user), trigger.sender)

