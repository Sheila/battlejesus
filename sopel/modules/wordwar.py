#!/usr/bin/env python
"""
wordwar.py - Word War Bot module
Author: mat, fix0rs by Phil
About: http://jesus.thewrigro.com
"""

import time, re, threading, random, wordgen

from flags import checkflags, AdminOnly
from functions import *
from util import minute_str

wars_on = True

wordwars = {#Format for WordWars:
    # id = list key (count(wordwars))
    # {"channel": Channel the war was begun in,
    #  "nick": User who started the wordwar
    # "duration": Duration of the word war, in minutes
    # "active": Whether or not the word war is active: 0, inactive, 1, active
    # "trigger": The current trigger status:
    #    0: waiting for the 60 second warning
    #    1: waiting for the start of word war warning
    #    2: waiting for the half-way warning
    #    3: waiting for the three-quarter way warning
    #    4: waiting for the end of war notice,
    # "warning": A list of integers of seconds
    #    [sixty second warning (0 = no warning), until start
    #     start of word war warning, until start
    #     half-way warning, from start
    #     three-quarter warning, from half-way
    #     end of war notice, frmo three-quarter],
    # "final": The unix timestamp of the final time,
    # "join": a list of usernames who have 'joined' the war; if a username starts with 'eo@', only the end message is sent.}
}

lastwars = {#Format for lastwar:
    # 'channel' (= Key),
    # 'name' (= of Wordwar)
}

def append_logfile (message, newline=True):
    logfile = open('/home/due/battlejesus/trunk/wordwar.log', 'a')
    if isinstance(message, list):
        if newline:
            message = '\n\n'.join(message)
        else:
            message = '\n'.join(message)
    elif isinstance(message, str):
        if newline:
            message += '\n'
    else:
        raise Exception, 'Invalid message type: %s.' % type(message)
    message = message.replace('^t', str(time.time()))
    logfile.write(message)
    logfile.close()

# Warning 0:
def warning_before (bot, id):
    """First warning to be triggered by threading.Timer in a Word War. This warning is only triggered if there are more than sixty seconds until the start of the word war. (See f_startwar)"""
    global wordwars
    append_logfile('^t: Warning 0 for %s.' % id)
    try:
        if wordwars[id]['active']:
            mywar = wordwars[id]
            if mywar['trigger'] == 0:
                append_logfile('^t: %s sending to %s people.' % (id, len(mywar['joined'])))
                war_str = "WORD WAR (%s, %s long): You have sixty seconds until the start of the word war." % (id, minute_str(mywar['duration']))
                bot.msg(mywar['channel'], war_str)
                for user in mywar['joined']:
                    if not user.startswith("eo@"):
                        bot.msg(user, war_str)
                wordwars[id]['trigger'] += 1
    except KeyError:
        return

# Warning 1:    """Start a wordwar in x minutes lasting y minutes"""
def warning_start (bot, id):
    """Second warning to be triggered. This one, like all of the ones that follow, is always triggered."""
    global wordwars
    append_logfile('^t: Warning 1 for %s.' % id)
    try:
        if wordwars[id]['active']:
            mywar = wordwars[id]
            if mywar['trigger'] == 1:
                append_logfile('^t: %s sending to %s people.' % (id, len(mywar['joined'])))
                war_str = "WORD WAR START: YOU HAVE %s. (%s)" % (minute_str(mywar['duration']).upper(), id)
                bot.msg(mywar['channel'], war_str)
                for user in mywar['joined']:
                    if user.startswith("eo@"):
                        user = user[3:len(user)]
                    bot.msg(user, war_str)
                warning2 = threading.Timer(mywar['warning'][2], warning_half, args=(bot, id))
                warning2.setDaemon(True)
                warning2.start()
                wordwars[id]['trigger'] += 1
    except KeyError:
        return

# Warning 2:
def warning_half (bot, id):
    """Third warning. See above."""
    global wordwars
    append_logfile('^t: Warning 2 for %s.' % id)
    try:
        if wordwars[id]['active']:
            mywar = wordwars[id]
            if mywar['trigger'] == 2:
                append_logfile('^t: %s sending to %s people.' % (id, len(mywar['joined'])))
                war_str = "WORD WAR: HALF WAY THROUGH WORD WAR. (%s, %s left)" % (id, minute_str(mywar['duration'] / 2.0))
                bot.msg(mywar['channel'], war_str)
                for user in mywar['joined']:
                    if not user.startswith("eo@"):
                        bot.msg(user, war_str)
                warning3 = threading.Timer(mywar['warning'][3], warning_threequarters, args=(bot, id))
                warning3.setDaemon(True)
                warning3.start()
                wordwars[id]['trigger'] += 1
    except KeyError:
        return

# Warning 3:
def warning_threequarters (bot, id):
    """Fourth warning. See above."""
    global wordwars
    append_logfile('^t: Warning 3 for %s.' % id)
    try:
        if wordwars[id]['active']:
            mywar = wordwars[id]
            if mywar['trigger'] == 3:
                append_logfile('^t: %s sending to %s people.' % (id, len(mywar['joined'])))
                war_str = "WORD WAR: THREE QUARTERS THROUGH WORD WAR. (%s, %s left)" % (id, minute_str(mywar['duration'] / 4.0))
                bot.msg(mywar['channel'], war_str)
                for user in mywar['joined']:
                    if not user.startswith("eo@"):
                        bot.msg(user, war_str)
                warning4 = threading.Timer(mywar['warning'][4], warning_final, args=(bot, id))
                warning4.setDaemon(True)
                warning4.start()
                wordwars[id]['trigger'] += 1
    except KeyError:
        return

# Warning 4:
def warning_final (bot, id):
    """Fifth and final warning. See above."""
    global wordwars
    append_logfile('^t: Warning 4 for %s.' % id)
    try:
        if wordwars[id]['active']:
            mywar = wordwars[id]
            if mywar['trigger'] == 4:
                append_logfile('^t: %s sending to %s people.' % (id, len(mywar['joined'])))
                bot.msg(mywar['channel'], "WORD WAR (%s) IS OVER." % id)
                for user in mywar['joined']:
                    if user.startswith("eo@"):
                        user = user[3:len(user)]
                    bot.msg(user, "WORD WAR (%s) IS OVER." % id)
                del wordwars[id]
    except KeyError:
        return

@AdminOnly
def f_stopall(self, origin, match, args):
    """*Stop all ongoing wordwars*, Usage: ``!stopall``

- *!stopall*       Stop all currently ongoing word wars in all channels and private messages.

"""
    global wordwars
    for i in wordwars.keys():
        del wordwars[i]
    self.msg(origin.sender, 'Stopped all ongoing word wars.')
f_stopall.rule = (['stopall'], r'(.*)')

@AdminOnly
def f_kill(self, origin, match, args):
    """*Kill a word war*, Usage: ``!kill [#]``

- *!kill [#]*       Stops the word war [#].

"""
    global wordwars
    id = match.group(1).strip()
    if wordwars.has_key(id):
        mywar = wordwars[id]
    else:
        mywar = None
    if checkflags(origin, '*') or (mywar is not None and mywar['nick'] == origin.nick):
        if mywar is not None:
            del wordwars[id]
            self.msg(origin.sender, "WORD WAR: Word War %s has been cancelled." % id)
        else:
            self.msg(origin.sender, "WORD WAR: Word War %s is not current." % id)

f_kill.rule = (['kill', 'peace'], r'([a-z]+)')

def f_status(self, origin, match, args):
    """*Word War status*, Usage: ``!status``

- *!status*       Returns information about word wars currently active in the channel via private message.

"""
    global wordwars
    ongoing = False
    for id in wordwars.keys():
        wordwar = wordwars[id]
        if wordwar['active']:
            if origin.sender.lower() == wordwar['channel'] or checkflags(origin, '*'):
                if wordwar['trigger'] > 1:
                    # Wordwar has started
                    ongoing = True
                    self.msg(origin.nick, "WORD WAR: '%s' in %s, started at %s by %s, ending in %.2f minutes." % (id, wordwar['channel'], time.strftime("%H:%M", time.gmtime(wordwar['start'])), wordwar['nick'], ((wordwar['final'] - time.time()) / 60)))
                else:
                    # Wordwar hasn't started yet
                    ongoing = True
                    self.msg(origin.nick, "WORD WAR: '%s' in %s, begun by %s, lasting %s, to start in %.2f minutes at %s." % (id, wordwar['channel'], wordwar['nick'], minute_str(wordwar['duration']), ((wordwar['start'] - time.time()) / 60), time.strftime("%H:%M", time.gmtime(wordwar['start']))))

    if not ongoing:
        self.msg(origin.nick, "WORD WAR: There are no current ongoing wordwars.")

f_status.rule = (['status'], r'(.*)')

@AdminOnly
def f_switchwar(self, origin, match, args):
    """*Disable Word Wars*, ``Usage: !switchwar (ON|OFF)``

- *!switchwar on*       Enable word wars.
- *!switchwar off*       Disable word wars.
- *!switchwar*       Return current start of word wars (enabled or disabled).

"""
    global wars_on
    if match.group(1):
        if match.group(1).lower() == 'on':
            wars_on = True
            self.msg(origin.nick, "WORD WARS: Word wars have been enabled.")
        elif match.group(1).lower() == 'off':
            wars_on = False
            self.msg(origin.nick, "WORD WARS: Word wars have been disabled.")
    else:
        if wars_on:
            self.msg(origin.nick, "WORD WARS: Switch is currently on.")
        else:
            self.msg(origin.nick, "WORD WARS: Switch is currently off.")

f_switchwar.rule = (['switchwar'], r'([Oo][Nn]|[Oo][Ff][Ff])?')

def f_startwar(self, origin, match, args):
    """*Start a WordWar*, Usage: ``!startwar # ##``

- *!startwar # ##*       Start a ## (maximum of 60) minute(s) long WordWar in # (maximum of 60) minute(s).

"""
    global wordwars, wars_on, word_syllables, lastwars
    if not wars_on and not checkflags(origin, '*'):
        self.msg(origin.sender, "WORD WAR: Starting new wars has been disabled temporarily.")
        return
    match2 = re.compile(r'([0-9]+) ([0-9]+)').match(match.group(1))
    try:
        start = int(match2.group(1))
        duration = int(match2.group(2))
    except:
        return
    if origin.sender:
        if start <= 60:
            if duration <= 60 and duration > 0:
                warning = []
                # Calculate them differently:
                # Warning: seconds from now
                warning.append((start * 60 - 60))
                # Start: seconds from now
                warning.append((start * 60))
                # Halfway warning: seconds from Start
                warning.append(((duration * 60) / 2))
                # Three quarter time: seconds from halfway warning
                warning.append(((duration * 60) / 4))
                # Stop time, seconds from three quarter warning
                warning.append(warning[3])
                # Calculate the final time as a unix timestamp
                final_time = (duration * 60) + (start * 60) + time.time()
                start_time = time.time() + (start * 60)

                # Prepare the war structure.
                war = {
                    "channel": origin.sender.lower(),
                    "nick": origin.nick,
                    "duration": duration,
                    "active": True,
                    "warning": warning,
                    "start": start_time,
                    "final": final_time,
                    "joined": [],
                }

                # If it's going to be more than a minute before it starts,
                # the first warning we display is warning 0 ("60 seconds...")
                # Otherwise, skip to the first one ("STARTED").
                if start > 1:
                    war["trigger"] = 0
                else:
                    war["trigger"] = 1

                id = wordgen.new_word(random.choice([4, 5, 6, 7]))
                while wordwars.has_key(id):
                   id = wordgen.new_word(random.choice([4, 5, 6, 7]))
                lastwars[origin.sender.lower()] = id
                wordwars[id] = war
                bot = self
                warning0 = threading.Timer(warning[0], warning_before, args=(bot, id))
                warning0.setDaemon(True)
                warning1 = threading.Timer(warning[1], warning_start, args=(bot, id))
                warning1.setDaemon(True)
                self.msg(origin.sender, 'WORD WAR (%s) WILL BEGIN IN %s, LASTING %s.' % (id, minute_str(start).upper(), minute_str(duration).upper()))
                # If it'll be more than a minute before the
                # war starts, we want to give the 60-second
                # notification.  Either way, we definitely
                # want to do the initial notification.
                now = time.time()
                w0 = now + warning[0]
                w1 = now + warning[1]
                w2 = now + warning[1] + warning[2]
                w3 = now + warning[1] + warning[2] + warning[3]
                w4 = now + warning[1] + warning[2] + warning[3] + warning[4]
                append_logfile('^t: Started wordwar %s.' % id)
                append_logfile('^t: Wordwar %s warning 0 at: %s' % (id, str(w0)))
                append_logfile('^t: Wordwar %s warning 1 at: %s' % (id, str(w1)))
                append_logfile('^t: Wordwar %s warning 2 at: %s' % (id, str(w2)))
                append_logfile('^t: Wordwar %s warning 3 at: %s' % (id, str(w3)))
                append_logfile('^t: Wordwar %s warning 4 at: %s' % (id, str(w4)))
                if start > 1:
                        warning0.start()
                warning1.start()

            else:
                self.msg(origin.sender, "WORD WAR: Invalid/non-numeric duration time.")
        else:
            self.msg(origin.sender, "WORD WAR: Invalid/non-numeric start time.")

f_startwar.rule = (['startwar', 'wordwar'], r'(.*)')

def f_help(self, origin, match, args):
    """*Help message*, Usage: ``!help``

- *!help*       Displays a basic !startwar and !status help message.

"""

    pipe = re.compile(r'>? ?(.*)').match(match.group(0))
    if pipe.group(1):
        if checkflags(origin, '*'):
            target = pipe.group(1)
        else:
            target = origin.nick
    else:
        target = origin.nick
    self.msg(target, "WORD WAR HELP: A word war is a set period of time where you stop procrastinating and write as many words as you possibly can. To start a word war, type !startwar (how many minutes until you want it to start [max 15]) (how many minutes you want it to last [max 30]).")
    self.msg(target, "To see ongoing Word Wars, type !status in a channel that the bot is in. Please note that it will only show Word Wars for that channel. For advanced functions, please see http://nw.aerdan.org/bj.html.")

f_help.rule = (['help'], r'(.*)')

def f_time(self, origin, match, args):
    """*Time*, Usage: ``!time``

- *!time*       Returns the current time of the bot (EST).

"""
    self.msg(origin.nick, "WORD WAR: The current time is %s (BLT)." % time.strftime("%H:%M:%S"))

f_time.rule = (['time'], r'(.*)')

def f_joinwar(self, origin, match, args):
    """*Join a WordWar*, Usage: ``!joinwar [eo@]war``

- *!joinwar [war]*       Join in the WordWar named war. Joining a WordWar will send you, in private message, the same notices that the bot sends to the channel.
- *!joinwar eo@[war]*       Join in the WordWar named war, but only recieve the starting message and the ending message in private message.

"""
    global wordwars
    name = match.group(1).lower()
    nick = origin.nick
    if name.startswith("eo@"):
        name = name[3:len(name)]
        nick = "eo@" + nick
    if wordwars.has_key(name):
        if nick in wordwars[name]['joined']:
            self.msg(origin.nick, "WORD WAR: You're already joined to word war '%s'." % name)
        else:
            wordwars[name]['joined'].append(nick)
            self.msg(origin.nick, "WORD WAR: You've been joined to word war '%s'." % name)
    else:
        self.msg(origin.nick, "WORD WAR: You cannot join '%s', as it does not exist!" % name)

f_joinwar.rule = (['joinwar'], r'([^ ]*)')

def f_joinwar_last(self, origin, match, args):
    """*Join a WordWar*, Usage: ``!joinwar-last [eo@]``

- *!joinwar-last*       Join the last WordWar for the channel, receiving all messages.
- *!joinwar-last eo@*       Join in the last WordWar for the channel, only receiving the starting and ending messages.

"""
    global lastwars, wordwars
    nick = origin.nick
    channel = origin.sender.lower()
    if match.group(1).lower().startswith("eo@"):
        nick = "eo@" + nick
    if lastwars.has_key(channel):
        id = lastwars[channel]
        if wordwars.has_key(lastwars[channel]):
            if wordwars[id]['active']:
                wordwars[id]['joined'].append(nick)
                self.msg(origin.nick, "WORD WAR: You've been joined to the most recent Word War for %s, '%s'." % (channel, id))
            else:
                self.msg(origin.nick, "WORD WAR: You cannot join the last war for '%s', as it is inactive!" % channel)
        else:
            self.msg(origin.nick, "WORD WAR: You cannot join '%s', as it does not exist!" % id)
    else:
        self.msg(origin.nick, "WORD WAR: You cannot join the previous wordwar for %s as it does not exist!" % channel)

f_joinwar_last.rule = (['joinwar-last'], r'([^ ]*)')

def f_partwar(self, origin, match, args):
    """*Part a WordWar*, Usage: ``!joinwar [war]``

- *!partwar [war]*       Stop receiving messages for word war <war>.

"""
    global wordwars

    name = match.group(1).lower()
    nick = origin.nick

    if wordwars.has_key(name):
        if nick in wordwars[name]['joined']:
           wordwars[name]['joined'].remove(nick)
           self.msg(origin.nick, "WORD WAR: You've been removed from word war '%s'." % name)
        else:
           self.msg(origin.nick, "WORD WAR: You cannot unjoin '%s', as you were not joined previously!" % name)
    else:
        self.msg(origin.nick, "WORD WAR: You cannot unjoin '%s', as it does not exist!" % name)

f_partwar.rule = (['partwar'], r'([^ ]*)')
