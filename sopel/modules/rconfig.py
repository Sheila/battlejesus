#!/usr/bin/env python
import sys
try:
    sys.path.append('../')
except:
    raise ImportError, 'Unable to reload configuration file.'
else:
    import config
