#!/usr/bin/env python
"""
energy.py - Container of mystical energy!
Author: Phil <phil SHIFT-2 thenexusproject UNSHIFT-LESSTHAN org>
About: http://jesus.thewrigro.com
"""

# Note that energy is host-based, not user-based.  This is mainly because
# user tracking across nicknames is a pain in the ass.  The added benefit
# of screwing over AOL proxy users is just, you know, an added benefit.

import time

from sopel.modules.flags import checkflags, admin_only
from sopel.modules.functions import energetic
from sopel.module import commands

class Energy (object):
    def __init__ (self, starting_energy = 100, max_energy = 100, recovery_rate = 0.20):
        self.energy = 0
        self.setMaxEnergy(max_energy)
        self.setEnergy(starting_energy)

        self.recovery_rate = recovery_rate
        self.last_check = time.time()
        self.last_burn = time.time()

    def normalizeEnergy (self):
        if self.energy < 0:
            self.energy = 0
        elif self.energy > self.max_energy:
            self.energy = self.max_energy

        self.energy = int(self.energy)
        self.max_energy = int(self.max_energy)

    def burnEnergy (self, amount):
        self.energy -= amount
        self.normalizeEnergy()
        self.last_burn = time.time()

    def refreshEnergy (self):
        current_time = time.time()

        # Potentially increase this user's max energy if it's been a long
        # time since they've burned energy.  It has to have been at least
        # an hour, and no one can have more than 200 maximum energy.
        if (current_time - self.last_burn > 3600 and self.max_energy < 200):
            additional_max = (current_time - self.last_burn) / 3600
            new_energy_amount = min(self.max_energy + additional_max, 200)
            self.setMaxEnergy(new_energy_amount)

            # We need to pretend like this burned energy, otherwise you
            # can really abuse this, alas.
            self.last_burn = current_time

        # Now, see how much of a refresh they get.
        time_difference = current_time - self.last_check
        amount_to_add = time_difference * self.recovery_rate
        if amount_to_add >= self.max_energy:
            self.setEnergy(self.max_energy)
        else:
            self.setEnergy(self.energy + amount_to_add)

        # Make sure to set the last_check time.
        self.last_check = current_time

    def setEnergy (self, amount):
        self.energy = amount
        self.normalizeEnergy()

    def setMaxEnergy (self, max_energy):
        self.max_energy = max_energy
        self.normalizeEnergy()

    def hasEnoughEnergy (self, cost):
        if self.energy >= cost:
            return True
        return False

class EnergyManager (object):
    def __init__ (self):
        self.hosts = {}

    def getEnergyForHost (self, host):

        if not host in self.hosts:
            new_energy = Energy()
            self.hosts[host] = new_energy

        return self.hosts[host]

energy_manager = EnergyManager()

def attempt_energetic_command(bot, origin, base_cost = 100, private_modifier = 10):
    if checkflags(bot, origin, '*'): # Free commands for all!
        base_cost = 0
        private_modifier = 0
    elif checkflags(origin, 'b'):
        return False

    user_energy = energy_manager.getEnergyForHost(origin.host)
    user_energy.refreshEnergy()
    actual_cost = base_cost
    if origin.sender[0] != '#': # Private message:
        if actual_cost:
           actual_cost /= private_modifier
    if user_energy.hasEnoughEnergy(actual_cost):
        user_energy.burnEnergy(actual_cost)
        return True
    else:

        # Deduct points for the attempt by dividing by private_modifier.
        # This means a failed private attempt probably costs nothing, which
        # is fine.
        if actual_cost:
           actual_cost /= private_modifier
        user_energy.burnEnergy(actual_cost)

        return False

@energetic(10, 10)
@commands('energy')
def energy(bot, trigger):
    user = energy_manager.getEnergyForHost(trigger.host)
    bot.say('ENERGY: You have {}/{} energy.'.format(user.energy, user.max_energy), trigger.nick)

@admin_only
@commands('giveenergy')
def giveenergy(bot, trigger):
    host = trigger.match.group(3)
    try:
        add_energy = int(trigger.match.group(4))
    except ValueError:
        bot.say("ENERGY: That's not a quantity I can give.", trigger.nick)

    if host not in energy_manager.hosts:
        bot.say("ENERGY: Couldn't find a user to give energy to.", trigger.nick)
    else:
        user = energy_manager.getEnergyForHost(host)
        user.setEnergy(add_energy)
        bot.say("ENERGY: Host '{}' energy now at {}/{}.".format(
                host, user.energy, user.max_energy
            ), trigger.nick
        )

