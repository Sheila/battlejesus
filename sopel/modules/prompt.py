#!/usr/bin/env python
"""
Prompt.py - A quick prompts engine, based on cq.py
"""

import re, db, admin, energy
from functions import *

from flags import checkflags

@Ignore_Channel_Only
@Energetic (25, 75)
def f_prompt (self, origin, match, args):
    """*Writing prompts*, Usage: ``!prompt [add|count|#|]``

- *!prompt add <text>*       Add the text of <text> to the prompt database.
- *!prompt count*       Displays the total number of prompts in the database.
- *!prompt #*       Display prompt number #.
- *!prompt*       Display a random prompt.

"""
    wdb = db.getcursor()
    # Multiple matches and formats:
    # !prompt add <text> (add the text as a random thingy)
    # !prompt <number> (get the text of quote <number>)
    # !prompt (get a random thingy)
    add = re.compile("([Aa][Dd][Dd]) (.*)")
    kill = re.compile("([Kk][Ii][Ll][Ll]) ([0-9]+)")
    count = re.compile("([Cc][Oo][Uu][Nn][Tt]|[Tt][Oo][Tt][Aa][Ll])")
    if match.group(0) == "":
        # random prompt
        wdb.execute("SELECT * FROM prompts ORDER BY RANDOM() LIMIT 1")
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "RANDOM PROMPT: %s (#%s)" % (row['prompt_text'].replace('\\', '').replace("''", "'"), row['prompt_id']))
        else:
            self.msg(origin.sender, "PROMPTS: An error occured. Please contact an admin.")
    elif match.group(0).isdigit():
        # numbered prompt
        wdb.execute("SELECT * FROM prompts WHERE prompt_id = ? LIMIT 1", (match.group(0),))
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "PROMPT #%s: %s" % (row['prompt_id'], row['prompt_text'].replace('\\', '').replace("''", "'")))
        else:
            self.msg(origin.sender, "PROMPTS: Prompt #%s does not exist." % match.group(0))
    elif add.match(match.group(0)):
        # add a quote
        if checkflags(origin, 'p'):
            prompt = add.match(match.group(0)).group(2)
            wdb.execute("INSERT INTO prompts VALUES (NULL, ?)", (prompt, ))
            if wdb.rowcount < 1:
                # Didn't succeed
                self.msg(origin.sender, "PROMPTS: Prompt failed. Please try again, or contact an admin.")
            else:
                # Yay!
                self.msg(origin.sender, "PROMPTS: Prompt added! Prompt is #%s." % db.insert_id(wdb))
    elif kill.match(match.group(0)):
        # remove a quote
        if checkflags(origin, 'p'):
            prompt = kill.match(match.group(0)).group(2)
            wdb.execute("DELETE FROM prompts WHERE prompt_id = ?", (prompt,))
            if wdb.rowcount < 1:
                # Didn't succeed
                self.msg(origin.nick, "Couldn't delete prompt %s, might not exist." % prompt)
            else:
                self.msg(origin.nick, "Deleted quote %s from the database." % prompt)
    elif count.match(match.group(0)):
        # count
        wdb.execute("SELECT COUNT(*) AS total FROM prompts LIMIT 1")
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "PROMPTS: There is a total of %s prompts in the database." % row['total'])
        else:
            self.msg(origin.sender, "PROMPTS: An error has occured, please contact an admin.")

    # Close the cursor:
    db.closecursor(wdb)
f_prompt.rule = (['prompt'], r'(.*)')

@Ignore_Channel_Only
@Energetic (25, 50)
def f_twist (self, origin, match, args):
    """*Writing twists*, Usage: ``!twist [add|count|#|]``

- *!twist add <text>*       Add the text of <text> to the twist database.
- *!twist count*       Displays the total number of twists in the database.
- *!twist #*       Display twist number #.
- *!twist*       Display a random twist.

"""
    wdb = db.getcursor()
    # Multiple matches and formats:
    # !twist add <text> (add the text as a random thingy)
    # !twist <number> (get the text of quote <number>)
    # !twist (get a random thingy)
    add = re.compile("([Aa][Dd][Dd]) (.*)")
    kill = re.compile("([Kk][Ii][Ll][Ll]) ([0-9]+)")
    count = re.compile("([Cc][Oo][Uu][Nn][Tt]|[Tt][Oo][Tt][Aa][Ll])")
    if match.group(0) == "":
        # random twist
        wdb.execute("SELECT * FROM twists ORDER BY RANDOM() LIMIT 1")
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "RANDOM TWIST: %s (#%s)" % (row['twist_text'].replace('\\', '').replace("''", "'"), row['twist_id']))
        else:
            self.msg(origin.sender, "TWISTS: An error occured. Please contact an admin.")
    elif match.group(0).isdigit():
        # numbered twist
        wdb.execute("SELECT * FROM twists WHERE twist_id = ? LIMIT 1", (match.group(0),))
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "TWIST #%s: %s" % (row['twist_id'], row['twist_text'].replace('\\', '').replace("''", "'")))
        else:
            self.msg(origin.sender, "TWISTS: twist #%s does not exist." % match.group(0))
    elif add.match(match.group(0)):
        # add a twist
        if checkflags(origin, 'p'):
            twist = add.match(match.group(0)).group(2)
            wdb.execute("INSERT INTO twists VALUES (NULL, ?)", (twist, ))
            if wdb.rowcount < 1:
                # Didn't succeed
                self.msg(origin.sender, "TWISTS: Plot twist failed. Please try again, or contact an admin.")
            else:
                # Yay!
                self.msg(origin.sender, "TWISTS: Plot twist added! It is #%s." % db.insert_id(wdb))

    elif kill.match(match.group(0)):
        # remove a quote
        if checkflags(origin, 'p'):
            twist = kill.match(match.group(0)).group(2)
            wdb.execute("DELETE FROM twists WHERE twist_id = ?", (twist,))
            if wdb.rowcount < 1:
                # Didn't succeed
                self.msg(origin.nick, "Couldn't delete twist %s, might not exist." % twist)
            else:
                self.msg(origin.nick, "Deleted quote %s from the database." % twist)
    elif count.match(match.group(0)):
        # count
        wdb.execute("SELECT COUNT(*) AS total FROM twists LIMIT 1")
        row = wdb.fetchone()
        if row is not None:
            self.msg(origin.sender, "TWISTS: There is a total of %s twists in the database." % row['total'])
        else:
            self.msg(origin.sender, "TWISTS: An error has occured, please contact an admin.")

    # Close the cursor:
    db.closecursor(wdb)

f_twist.rule = (['twist'], r'(.*)')
