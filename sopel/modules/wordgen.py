#!/usr/bin/env python
"""
   wordgen.py: A word generator.

   This particular nonsense written by Phil.
"""

import random

vowel_list = ['a', 'e', 'i', 'o', 'u']
def vowel():
  return random.choice(vowel_list)

def vowel_pair():
   return vowel() + vowel()

SINGLE_VOWEL_PROBABILITY = 0.8
def V(initial = False):
   if initial or random.random() < SINGLE_VOWEL_PROBABILITY:
      return vowel()
   else:
      return vowel_pair()

consonant_list = ['b', 'c', 'd', 'f', 'g', 'h',
                  'j', 'k', 'l', 'm', 'n', 'p',
                  'r', 's', 't', 'v', 'w', 'x',
                  'y', 'z']
def consonant():
   return random.choice(consonant_list)

digraph_list = ['ch', 'wh', 'th', 'sh', 'ng',
                'kn', 'ph', 'gh', 'ck']

starting_digraph_list = ['ch', 'wh', 'th', 'sh',
                         'kn', 'ph', 'gh']

double_consonant_list = ['bb', 'cc', 'dd', 'ff', 'gg',
                         'll', 'mm', 'nn', 'pp', 'rr',
                         'ss', 'tt', 'zz']

DIGRAPH_PROBABILITY = 0.75
def consonant_pair(initial = False):
   if random.random() < DIGRAPH_PROBABILITY:
      if initial:
         return random.choice(starting_digraph_list)
      else:
         return random.choice(digraph_list)
   else:
      return random.choice(double_consonant_list)

SINGLE_CONSONANT_PROBABILITY = 0.95
def C(initial = False):
   if random.random() < SINGLE_CONSONANT_PROBABILITY:
      return consonant()
   else:
      return consonant_pair(initial)

VOWEL_START_PROBABILITY = 0.5
def new_word (min_length = 5):
   word = ""
   start = True
   if random.random() < VOWEL_START_PROBABILITY:
      state = "V"
   else:
      state = "C"
   while len(word) < int(min_length):
      if state == "V":
         word += V(start)
         state = "C"
      elif state == "C":
         word += C(start)
         state = "V"
      start = False

   return word

if "__main__" == __name__:
   for i in range(3):
      for j in range(10):
         print new_word(j)
