#!/usr/bin/env python
"""
reload.py - Reload Modules Dynamically
Author: Sean B. Palmer, inamidst.com
About: http://inamidst.com/phenny/
"""

import irc, os, sys, subprocess
from flags import AdminOnly, WithFlag
from functions import *

@WithFlag('d')
def f_rehash(self, origin, match, text):
    """*Reload all modules*, Usage: ``!rehash``

- *!rehash*       Reload all currently registered modules.

"""
    modules = []
    error = []
    module = {}
    moduledir = os.path.join(self.home, 'modules')
    for filename in sorted(os.listdir(moduledir)): 
        if filename.endswith('.py') and not filename.startswith('_'): 
            name, ext = os.path.splitext(os.path.basename(filename))
            try: module = getattr(__import__('modules.' + name), name)
            except Exception, e: 
                error.append(name)
            else: 
                reload(module)
                self.register(vars(module))
                self.bindrules()
                modules.append(name)
    if modules: 
        if error == []:
            error.append('none')
        self.msg(origin.sender, "Rehashed modules: "+', '.join(modules)+". Failed to load: "+", ".join(error))
        
    import rconfig
    config = rconfig.config
    for channel in config.channels_in:
        self.write(('NAMES', channel))

f_rehash.rule = (['rehash'], r'(.*)')

@WithFlag('d')
def f_reload(self, origin, match, text):
    """*Reload a module*, Usage: ``!reload [module] [, [module] [, ... ]]``

- *!reload [module]*       Reload module [module]. [module] must be a valid module.
- *!reload [module], [module]...*       Reload multiple modules, in the comma-separated format. must be a valid modules.

""" 
    name = match.group(1)
    names = []
    if ',' in name:
        names = [x.strip() for x in name.split(',')]
    else:
        names = [name]

    for name in names:
        module = getattr(__import__('modules.' + name), name)
        reload(module)
        if hasattr(module, '__file__'): 
            import os.path, time
            mtime = os.path.getmtime(module.__file__)
            modified = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(mtime))
        else: modified = 'unknown'

        self.register(vars(module))
        self.bindrules()

        msg = 'Reloaded module %r (%s)' % (name, modified)
        self.msg(origin.sender, msg)
        
    if 'logging' in names:
        import rconfig
        config = rconfig.config
        for channel in config.channels_in:
            self.write(('NAMES', channel))
        
f_reload.rule = (['reload'], r'(.*)')

@Threaded
@AdminOnly
def f_sync(self, origin, match, text):
    """*Sync with subversion*, Usage: ``!sync``

- *!sync*       Update the bot to the current subversion version.

"""
    # Updating Subversion may take a while.  Let them know we're doing this.
    self.msg(origin.sender, "Updating via SVN...")

    read_pipe, write_pipe = os.pipe()
    os.chdir(self.startup)
    text = subprocess.Popen('svn up', shell=True, stdout=os.fdopen(write_pipe, 'w'))
    os.chdir("/")
    data = os.fdopen(read_pipe).read()
    self.msg(origin.sender, "SVN updated with: %s" % data.replace("\n", ' '))

f_sync.rule = (['sync'], r'(.*)')
